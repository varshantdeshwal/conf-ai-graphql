 const {gql}= require('apollo-server');

 const Schema = gql`
type Room {
    _id: String
    name: String
    seating_capacity: Int
}

type Booking {
    _id: String
    room: Room
    user: User
    purpose: String
    date: String
    slots: [Int]
}
type User{
    _id: String
    name: String
    email: String
}


type Query {
    booking(bookingId: String): Booking
    userBookings(userId: String): [Booking]
    roomBookings(roomId: String, date: String): [Booking]
    rooms: [Room]
}

type Mutation {
    signup(email: String, name:String, password: String): SignupResponse
    signin(email: String, password: String): SigninResponse
    addRoom(name: String, seatingCapacity: Int): RoomResponse
    deleteRoom(roomId: String): RoomResponse
    addBooking(roomId: String, slots:[Int], purpose: String, date: String, userId: String): BookingResponse
    deleteBooking(bookingId: String): BookingResponse
}

type BookingResponse {
    success: Boolean
    message: String
    booking: Booking
}

type SignupResponse {
    user: User
    message: String
}
type SigninResponse {
    user: User
    message: String
    token: String
}
type RoomResponse {
    room: Room
    message: String
}
`
module.exports = Schema