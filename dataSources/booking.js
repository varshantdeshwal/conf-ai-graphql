const {RESTDataSource} = require('apollo-datasource-rest');

class BookingAPI extends RESTDataSource {
    constructor(){
        super();
        this.baseURL = 'http://localhost:3048/booking/';
    }

    willSendRequest(request) {

      request.headers.set('Authorization', this.context.authorization);
  }
  async addBooking(body) {
    console.log('add booking called');
      const response = await this.post('/add', body)
      return response;
  }
  async deleteBooking({bookingId}) {
    console.log('delete booking called');
      const response = await this.delete(`/booking/${bookingId}`)
      return response;
  }
  async fetchRoomBookings({roomId, date}) {
    console.log('fetch room bookings called called');
      const response = await this.get(`/room/${roomId}/${date}`);
      return response.bookings;
  }
  async fetchUserBookings({userId}) {
    console.log('fetch user bookings called called');
    const response = await this.get(`/user/${userId}`);
    return response.bookings;
}
async fetchBooking({bookingId}) {
    console.log('fetch booking called');
    const response = await this.get(`/booking/${bookingId}`);
    return response.booking;
}
}

module.exports = BookingAPI;