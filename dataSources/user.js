const {RESTDataSource} = require('apollo-datasource-rest');

class UserAPI extends RESTDataSource {
    constructor(){
        super();
        this.baseURL = 'http://localhost:3048/user/';
    }

    // initialize(config) {
    //     console.log(config);
    //     this.context = config.context;
    //   }
    // willSendRequest(request) {
    //     request.headers.set('Authorization', `bearer ${this.context.token}`);
    //   }

    async signup(body){
        const response = await this.post('/signup', body)
        return response;
            }
    
    async signin(body){
        const response = await this.post('/signin', body)
        return response;
            }

    userReducer(response) {
        return {
            user: response.user,
            message: response.message
        }
    }


}

module.exports = UserAPI;