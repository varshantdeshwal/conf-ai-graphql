const { RESTDataSource } = require('apollo-datasource-rest');

class RoomAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = 'http://localhost:3048/room/';
    }

    // initialize(config) {
    //     this.context = config.context;
    // }
    willSendRequest(request) {
        request.headers.set('authorization', this.context.authorization);
    }
    async addRoom(body) {
        console.log('add room called');
        const response = await this.post('/', body)
        return response;
    }
    async deleteRoom({roomId}) {
        console.log('delete room called');
        const response = await this.delete(`/${roomId}`)
        return response;
    }
    async fetchAllRooms() {
        console.log('fetch all room called');
        const response = await this.get('/all');
        return response.rooms;
    }


}

module.exports = RoomAPI;