const {ApolloServer} = require('apollo-server'); 
const typeDefs = require ('./schema');
const checkAuth = require("./check-auth");
const UserAPI = require('./dataSources/user')
const RoomAPI = require('./dataSources/room')
const BookingAPI = require('./dataSources/booking');
const resolvers = require('./resolver');
const getContext = ({req}) => {
  if (req && req.headers) {
    return {
      authorization: req.headers['authorization'],
    };
  } else {
    return {};
  }
};

// const checkAuth = async (req, res, next) => {
//   await fetch("http://localhost:3048/auth", {
//     method: "GET",
//     headers: {
//       Accept: "application/json",
//       "Content-Type": "application/json",
//       Authorization: "Bearer " + req.headers['authorization']
//     }
//   });
//   const response = await res.json();
//   if(response.message === "auth failed"){
// return 
//   }
// };
const server = new ApolloServer({typeDefs,resolvers, dataSources: () => ({userAPI: new UserAPI(), roomAPI: new RoomAPI(), bookingAPI: new BookingAPI()}), context: getContext });

server.listen(8000).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
  });